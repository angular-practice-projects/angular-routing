import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest} from 'rxjs';
import { map } from 'rxjs/operators';
import { GithubFollowerService } from '../services/github-follower-service';


@Component({
  selector: 'app-github-profile',
  templateUrl: './github-profile.component.html',
  styleUrls: ['./github-profile.component.css']
})
export class GithubProfileComponent implements OnInit {

  constructor(private route: ActivatedRoute, private service: GithubFollowerService) { }
  
  ngOnInit(): void {

    combineLatest([
      this.route.paramMap,
      this.route.queryParamMap,
    ])
    .pipe(map(combined => {
      let unm = combined[0].get('username');
      let uid = combined[0].get('userid');

      return this.service.getFollowers(unm);
    }))
    .subscribe(followers => {
      console.log(followers);
    });
  }
}

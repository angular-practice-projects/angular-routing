import { Component, OnInit } from '@angular/core';
import { GithubFollowerService } from '../services/github-follower-service';

@Component({
  selector: 'app-github-followers',
  templateUrl: './github-followers.component.html',
  styleUrls: ['./github-followers.component.css']
})
export class GithubFollowersComponent {

  username: string;

  constructor(private followerService: GithubFollowerService) {

      this.username = "mitchtabian";
      this.getFollowers()
   }

  private _followers:any;

  getFollowers(){
    this.followerService.getFollowers(this.username)
      .subscribe(response => {
        console.log(response)
        this._followers = response;
      },
      error => {
          return error;
      });
  }

  get followers(){
    return this._followers;
  }
}

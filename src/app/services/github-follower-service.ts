import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class GithubFollowerService{

    private url = "https://api.github.com/users/";

    constructor(private http:HttpClient){}

    getFollowers(name:string){
        let endpoint:string = this.url + name + '/followers';

        return this.http.get(endpoint);
    }
}